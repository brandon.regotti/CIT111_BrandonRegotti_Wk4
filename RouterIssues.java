package Week4Assignment;

import java.util.Scanner;

public class RouterIssues {

	public static void main(String[] args) {
		
		//Variable to hold the user's responses
		String response;
		
		//Create a Scanner objects
		Scanner keyboard = new Scanner(System.in);

		//Display an introduction message
		System.out.println("This app will help you resolve a bad wifi connection");
		
		//This will start the question
		System.out.println("Perform this action: Reboot the computer and try to connect."); //option 1
		System.out.print("Did that resolve your issue? (Enter yes or no): ");
		response = keyboard.nextLine();
		if(response.equals("no"))
		{
			System.out.println("Perform this action: Reboot the router and try to connect."); //option 2
			System.out.print("Did that resolve your issue? (Enter yes or no): ");
			response = keyboard.nextLine();
			if(response.equals("no"))
			{
				System.out.println("Perform this action: Check that the wires connecting the router are not loose."); //option 3
				System.out.println("Did that resolve your issue? (Enter yes or no): ");
				response = keyboard.nextLine();
				if(response.equals("no"))
				{
					System.out.println("Perform this action: Completely unplug the router and re plug all of the wires in."); //Option 4
					System.out.println("Did that resolve your issue? (Enter yes or no): ");
					response = keyboard.nextLine();
					if(response.equals("no"))
					
					{
						System.out.println("It's time to purchase a new router."); //Last Statement
					}
					if(response.equals("yes"))
					{
						System.out.println("I am glad to see your issue could be resolved.");
					}
									
				}	
			}
		}
		
		
		}
	}


